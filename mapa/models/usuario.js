
// Modelo de usuario

var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

// Crea el esquema de la base de datos...
var usuarioSchema = new Schema({
	nombre : String,
});


// Crea el metodo de alta de reserva....
usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
	var reserva = new Reserva({usuario: this.id, bicicleta: biciId, desde: desde, hasta: hasta});
	console.log(reserva);
	reserva.save(cb);
};

module.exports = mongoose.model('usuario', usuarioSchema)